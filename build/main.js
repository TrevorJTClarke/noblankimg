require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("stream");

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(5);


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

__webpack_require__(6);

var _server = __webpack_require__(8);

var _server2 = _interopRequireDefault(_server);

var _routes = __webpack_require__(16);

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = new _server2.default().router(_routes2.default).listen(parseInt(process.env.PORT, 10));

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _dotenv = __webpack_require__(7);

var _dotenv2 = _interopRequireDefault(_dotenv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_dotenv2.default.config();

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("dotenv");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(2);

var _express2 = _interopRequireDefault(_express);

var _path = __webpack_require__(0);

var path = _interopRequireWildcard(_path);

var _bodyParser = __webpack_require__(9);

var bodyParser = _interopRequireWildcard(_bodyParser);

var _http = __webpack_require__(10);

var http = _interopRequireWildcard(_http);

var _os = __webpack_require__(11);

var os = _interopRequireWildcard(_os);

var _cors = __webpack_require__(12);

var _cors2 = _interopRequireDefault(_cors);

var _cookieParser = __webpack_require__(13);

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _logger = __webpack_require__(14);

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = new _express2.default();

class Server {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);
    app.use(bodyParser.json({ limit: '5mb' }));
    app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));
    app.use((0, _cookieParser2.default)(process.env.SESSION_SECRET));
    app.use(_express2.default.static(`${root}/public`));
    app.use((0, _cors2.default)());
    app.disable('x-powered-by');
    app.enable('case sensitive routing');
    app.enable('strict routing');
  }

  router(routes) {
    routes(app);
    return this;
  }

  listen(port = parseInt(process.env.PORT, 10)) {
    const welcome = p => () => _logger2.default.info(`up and running in ${"development" || 'development'} @: ${os.hostname()} on port: ${p}}`);
    http.createServer(app).listen(port, welcome(port));
    return app;
  }
}
exports.default = Server;
/* WEBPACK VAR INJECTION */}.call(exports, "server/common"))

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("os");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("cookie-parser");

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pino = __webpack_require__(15);

var _pino2 = _interopRequireDefault(_pino);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const l = (0, _pino2.default)({
  name: process.env.APP_ID || 'APP',
  level: process.env.LOG_LEVEL || 1
});

exports.default = l;

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("pino");

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = routes;

var _router = __webpack_require__(17);

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const baseUri = '/v1';

function routes(app) {
  app.use(`${baseUri}/assets`, _router2.default);
}

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = __webpack_require__(2);

var express = _interopRequireWildcard(_express);

var _multer = __webpack_require__(18);

var _multer2 = _interopRequireDefault(_multer);

var _controller = __webpack_require__(19);

var _controller2 = _interopRequireDefault(_controller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

// handle multi-part
const upload = (0, _multer2.default)();

exports.default = express.Router().post('/', upload.any(), _controller2.default.create).get('/:id/fallback', _controller2.default.byIdImageFallback).get('/:id/meta', _controller2.default.metaById).get('/:id', _controller2.default.byId).patch('/:id', _controller2.default.updateId).delete('/:id', _controller2.default.removeId);

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = require("multer");

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = __webpack_require__(1);

var _fs2 = _interopRequireDefault(_fs);

var _path = __webpack_require__(0);

var _path2 = _interopRequireDefault(_path);

var _stream = __webpack_require__(3);

var _stream2 = _interopRequireDefault(_stream);

var _assets = __webpack_require__(20);

var _assets2 = _interopRequireDefault(_assets);

var _assetMeta = __webpack_require__(23);

var _assetMeta2 = _interopRequireDefault(_assetMeta);

var _ipfs = __webpack_require__(25);

var _ipfs2 = _interopRequireDefault(_ipfs);

var _web = __webpack_require__(29);

var _web2 = _interopRequireDefault(_web);

var _amberdata = __webpack_require__(31);

var _amberdata2 = _interopRequireDefault(_amberdata);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const imgManifest = __webpack_require__(33);
const Assets = new _assets2.default();
const AssetMeta = new _assetMeta2.default();
const Amberdata = new _amberdata2.default();
const ipfs = new _ipfs2.default();
const web3 = new _web2.default();

const handleAssetType = hash => {
  const hasDot = hash.includes('.');
  return hasDot ? hash.split('.') : [hash];
};

const apiUrl =  false ? 'https://api.noblankimg.com/v1' : 'http://localhost:1337/v1';

const getAssetUrl = hash => {
  return `https://ipfs.infura.io:5001/api/v0/object/data?arg=${hash}`;
};

const getAssetPath = hash => {
  return `${apiUrl}/assets/${hash}.png`;
};

class Controller {
  async create(req, res) {
    // TODO: Validate data!!!!
    // TODO: Strip the script tags from svg
    const data = await ipfs.upload(new Buffer.from(req.files[0].buffer));
    res.json(data);
  }

  async byId(req, res) {
    const C = new Controller();
    const hash = req.params.id;
    const options = {
      w: req.query && req.query.w ? req.query.w : 128,
      h: req.query && req.query.h ? req.query.h : 128
      // options.path = path.join(__dirname, '../../../../static/test_image.jpg')

    };try {
      // If is address, return fallbackImage directly
      // Otherwise, check ipfs, if no image return fallbackImage
      // const addressData = await Amberdata.getAddress(hash)
      // console.log('addressData', addressData)
      // if (addressData.addressType === 'address') {
      //   return C.byIdImageFallback(req, res)
      // }

      // TODO: - get ipfs hash from web3, if none found return fallback
      // TODO: - get img from local cache
      // - if no cache, get from IPFS and cache, return asset
      // const ref = handleAssetType(hash)
      const ref = handleAssetType(hash);
      // console.log('ref', ref)
      const files = await ipfs.byHash(imgManifest[ref[0]]);

      if (!files) {
        return C.byIdImageFallback(req, res);
      }
      options.content = files[0].content;
      options.type = ref[1] || 'png';

      Assets.image(options).pipe(res);
    } catch (e) {
      res.send('');
    }
  }

  async metaById(req, res) {
    const hash = req.params.id;
    const options = {};
    try {
      const addressData = await Amberdata.getAddress(hash);
      addressData.assetPath = getAssetPath(hash);
      // console.log('addressData', addressData)

      // TODO: Get from static
      // const hashRef = await web3.getHashReference(hash)
      // addressData.verified = hashRef.verified
      // console.log('hashRef', hashRef)
      // if (!hashRef) {
      //   res.json(addressData)
      //   return
      // }

      // options.path = path.join(__dirname, `../../../../static/${hash}.png`)
      // console.log('imgManifest[hash]', imgManifest[hash])
      options.path = ipfs.getUrl(imgManifest[hash]);
      // console.log('options.path', options.path)

      // options.path = getAssetUrl(hashRef)
      const palette = await AssetMeta.image(options);
      addressData.brand = palette;
      // console.log('palette', palette)

      res.json(addressData);
    } catch (e) {
      console.log('e', e);
      res.send('[]');
    }
  }

  async byIdImageFallback(req, res) {
    try {
      const ref = handleAssetType(req.params.id);
      const fallback = await Assets.fallbackImage(ref[0]);
      const bufferStream = new _stream2.default.PassThrough();
      bufferStream.end(new Buffer.from(fallback));

      return bufferStream.pipe(res);
    } catch (e) {
      console.log('e', e);
      res.send('');
    }
  }

  updateId(req) {
    // TODO: is this needed?
    return '';
  }

  removeId(req) {
    // TODO: is this needed?
    return '';
  }
}

exports.default = new Controller();

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = __webpack_require__(1);

var _fs2 = _interopRequireDefault(_fs);

var _stream = __webpack_require__(3);

var _stream2 = _interopRequireDefault(_stream);

var _sharp = __webpack_require__(21);

var _sharp2 = _interopRequireDefault(_sharp);

var _jdenticon = __webpack_require__(22);

var _jdenticon2 = _interopRequireDefault(_jdenticon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class AssetsProvider {
  constructor() {
    return this;
  }

  fallbackImage(hash) {
    return _jdenticon2.default.toPng(hash, 256);
  }

  image(options) {
    const bufferStream = new _stream2.default.PassThrough();
    bufferStream.end(new Buffer.from(options.content));
    let s = (0, _sharp2.default)();

    s = s.resize(parseInt(options.w, 10), parseInt(options.h, 10));

    if (options.type === 'png') s = s.png();
    if (options.type === 'jpg' || options.type === 'jpeg') {
      s = s.jpeg({
        quality: 100,
        chromaSubsampling: '4:4:4'
      });
    }

    // TODO: Setup for invalidated images
    // .blur(30)
    // .toColourspace('b-w')

    return bufferStream.pipe(s);
  }
}

exports.default = AssetsProvider;

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("sharp");

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = require("jdenticon");

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
const getColors = __webpack_require__(24);

class AssetMetaProvider {
  constructor() {
    return this;
  }

  async image(options) {
    const colors = await getColors(options.path, 'image/png');
    return colors.map(color => color.hex());
  }
}

exports.default = AssetMetaProvider;

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = require("get-image-colors");

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ipfsApi = __webpack_require__(26);

var _ipfsApi2 = _interopRequireDefault(_ipfsApi);

var _multihash = __webpack_require__(27);

var _multihash2 = _interopRequireDefault(_multihash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Looks like IPFS on infura doesn't require API key yet
const apiKey = process.env.INFURA_API_KEY || '';

// TODO: Might not needed
// const fileOptions = {
//   pin: true,
//   wrapWithDirectory: 'test'
// }

class IpfsProvider {
  constructor() {
    this.baseUrl = 'https://ipfs.infura.io:5001/api/v0/cat?arg=';
    this.ipfs = (0, _ipfsApi2.default)('ipfs.infura.io', '5001', { protocol: 'https' });
    return this;
  }

  async upload(bufferData) {
    return new Promise((resolve, reject) => {
      this.ipfs.files.add(bufferData, async (err, files) => {
        if (err) return reject(err);
        const hash = files[0].hash;
        const fileData = await _multihash2.default.getBytes32FromMultiash(hash);
        fileData.hash = hash;

        resolve(fileData);
      });
    });
  }

  async byHash(hash) {
    return new Promise((resolve, reject) => {
      try {
        this.ipfs.files.get(hash, (err, files) => {
          if (err) return resolve(null);
          return resolve(files);
        });
      } catch (e) {
        reject(e);
      }
    });
  }

  getUrl(hash) {
    return `${this.baseUrl}${hash}`;
  }
}

exports.default = IpfsProvider;

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = require("ipfs-api");

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const bs58 = __webpack_require__(28);

const self = module.exports = {
  /**
   * @typedef {Object} Multihash
   * @property {string} digest The digest output of hash function in hex with prepended '0x'
   * @property {number} hashFunction The hash function code for the function used
   * @property {number} size The length of digest
   */

  /**
   * Partition multihash string into object representing multihash
   *
   * @param {string} multihash A base58 encoded multihash string
   * @returns {Multihash}
   */
  getBytes32FromMultiash(multihash) {
    const decoded = bs58.decode(multihash);
    const data = decoded.slice(2);
    const digest = `0x${Buffer.from(data, 'utf8').toString('hex')}`;

    return {
      digest,
      hashFunction: decoded[0],
      size: decoded[1]
    };
  },

  /**
   * Encode a multihash structure into base58 encoded multihash string
   *
   * @param {Multihash} multihash
   * @returns {(string|null)} base58 encoded multihash string
   */
  getMultihashFromBytes32(multihash) {
    const { digest, hashFunction, size } = multihash;
    if (size === 0) return null;

    // cut off leading "0x"
    const hashBytes = Buffer.from(digest.slice(2), 'hex');

    // prepend hashFunction and digest size
    const multihashBytes = new hashBytes.constructor(2 + hashBytes.length);
    multihashBytes[0] = hashFunction;
    multihashBytes[1] = size;
    multihashBytes.set(hashBytes, 2);

    return bs58.encode(multihashBytes);
  },

  /**
   * Parse Solidity response in array to a Multihash object
   *
   * @param {object} response Response array from Solidity
   * @returns {Multihash} multihash object
   */
  parseContractResponse(response) {
    const { digest, hashFunction, size } = response;
    return {
      digest,
      hashFunction,
      size
    };
  },

  /**
   * Parse Solidity response in array to a base58 encoded multihash string
   *
   * @param {array} response Response array from Solidity
   * @returns {string} base58 encoded multihash string
   */
  getMultihashFromContractResponse(response) {
    return self.getMultihashFromBytes32(self.parseContractResponse(response));
  }
};

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = require("bs58");

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = __webpack_require__(1);

var _fs2 = _interopRequireDefault(_fs);

var _path = __webpack_require__(0);

var _path2 = _interopRequireDefault(_path);

var _web = __webpack_require__(30);

var _web2 = _interopRequireDefault(_web);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const godAddress = '0x0000000000000000000000000000000000000000000000000000000000000000';
const ContractAbiPath = _path2.default.join(__dirname, '../../../', 'build/contracts/NoBlankImg.json');

// Get an API key from https://infura.io/
const apiKey = process.env.INFURA_API_KEY || '';
const network = process.env.ETH_NETWORK || '';
const rpcUrl = () => {
  if (true) return `http://localhost:8545`;
  return `https://${network}.infura.io/v3/${apiKey}`;
};

const getAbiDeployedAddress = abi => {
  const networks = abi.networks;
  return networks[Object.keys(networks)[0]].address;
};

class Web3Provider {
  constructor() {
    this.web3 = new _web2.default(new _web2.default.providers.HttpProvider(rpcUrl()));

    try {
      this.AbiFile = JSON.parse(_fs2.default.readFileSync(ContractAbiPath, 'utf8'));
      if (this.AbiFile) this.deployedAddress = getAbiDeployedAddress(this.AbiFile);
      this.Contract = new this.web3.eth.Contract(this.AbiFile.abi, this.deployedAddress);
    } catch (e) {
      console.log('e', e);
      console.error('Could not load contract ABI file, please check address or redeploy!');
    }

    return this;
  }

  getHashReference(hash) {
    return new Promise((resolve, reject) => {
      // this.Contract.methods.verifiedAsset(hash).call((e, r) => {
      //   if (e) return reject(e)
      //   if (r === godAddress) return resolve(null)
      //   resolve(r)
      // })
      this.Contract.methods.getAssetByHash(hash).call((e, r) => {
        if (e) return reject(e);
        console.log('r', r['0'], r['1'], _web2.default.utils.hexToAscii(r['0']));
        const hashRef = r['0'];
        const verified = r['1'] !== godAddress && r['1'] === hashRef;
        resolve({
          hash: hashRef,
          verified
        });
      });
    });
  }
}

exports.default = Web3Provider;
/* WEBPACK VAR INJECTION */}.call(exports, "server/api/providers"))

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = require("web3");

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _axios = __webpack_require__(32);

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const blockchainId = '1c9c969065fcd1cf'; // import fs from 'fs'

const baseUrl = `https://${blockchainId}.api.amberdata.io`;
const baseOptions = {
  headers: {
    'x-amberdata-blockchain-id': blockchainId
  }
};

class AmberdataProvider {
  constructor() {
    return this;
  }

  getAddress(hash) {
    return _axios2.default.get(`${baseUrl}/address/${hash}`, baseOptions).then(res => {
      const data = res.data;
      if (!data) return {};
      return {
        addressType: data.addressType,
        contractType: data.contractType,
        createdAt: data.createdAt,
        creationBlock: data.creationBlock
      };
    }).catch(() => {
      return {};
    });
  }
}

exports.default = AmberdataProvider;

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 33 */
/***/ (function(module, exports) {

module.exports = {"0xde083e40fe84835cbbd6c69f6595cae1e85551dc":"QmcVYunX95YRsRZWUxw97THVjtDRG2wcxL834TK65gGMtL","0x747616c4a19bd9bf1e2b6c8a77d206ea1f9c6018":"QmUuAt4Mwb4PovD63TruT5idU2sgHxmWAeACL8YAnVZpD9","0x96313f2c374f901e3831ea6de67b1165c4f39a54":"QmUcWj9QxktYK7bVLcrZTu5G1UyoGfQMDHRgUxgZzcpfUn","0x752c4e4e90846c5673c3791b9809f71b7d4a638a":"QmPAAKqVu5qVP2hxhhocn3VYcowxR2Z5r2R8Tf7hzCwVM6","0x552d72f86f04098a4eaeda6d7b665ac12f846ad2":"QmeHRQhYFtrh2ot6AWAX78c5wL6mTCMyi5pNe1aeKAEku1","0x13c8a7817ab3e74c28f5ef0eb7d7a3bdb329c2a0":"QmNr9GwX15XZdEFVNHAqYCTHY2R17x4SvrjcvJ2odBvjqR","0xa01a45394d98a67be493bcfc62a308847e6eb7fd":"QmVCrD3nokKCBRS9mGP9AaSRu91pdKCn1dbjWZN6NEDKwP","0xabc7e6c01237e8eef355bba2bf925a730b714d5f":"QmRRaoEqgD8tkHt2QJR7grHNYa26e93pS22Lb43YFxSvGh","0x5caebd3b32e210e85ce3e9d51638b9c445481567":"QmUaNt2LNQgWeYYKVFc2zzwtxdQgUVTBBgsAxH6vCxBBfE","0x87d598064c736dd0c712d329afcfaa0ccc1921a1":"QmP7wYDPF7eQqQiMCuij4jL9Fj7yn9gcxdW4P9ueSWDHYY","0x06012c8cf97bead5deae237070f9587f8e7a266d":"QmaQPeLh32Y72ffNPtwxE6ciwE5EozBLB4aXUiy6KgikCw","0x53abc335bed36e0299ef5607f04bd297d3a10dba":"Qmdh5Z2V3HF2QgtCmBozUDxYq3nz7q4oYXhJbNPtcgTx66","0xcfbc9103362aec4ce3089f155c2da2eea1cb7602":"QmZ8rC2htmaXtb52R8vsQsN2CqWiEMvWVX95RkjVva7QTj","0x0c6644c9973593b6a4c9c0af1af88f6b29ecace9":"QmcA56gBtuFeJdGUoM8ygrsvhNmXLUa4oQ9tNtKJRAcCkx","0xefabe332d31c3982b76f8630a306c960169bd5b3":"QmerPLNNnWArU4chqZLSfReAhhmWJBj6QR1qB5foWLXWDC","0xf7a6e15dfd5cdd9ef12711bd757a9b6021abf643":"QmafUBsYfpMCEdQkXNotHMA4tV2ZWAENXjA2B3BeUnkL8p","0x995020804986274763df9deb0296b754f2659ca1":"QmZwnaCRk8oMKxBYXMUVTG1Bo2jQzAPnrGrriiQ82rMJx2","0xa6d954d08877f8ce1224f6bfb83484c7d3abf8e9":"QmUExh9fFCZqZWDaqWX4s4btxqQteNjhdSwrcWXUz2rSr6","0xb47e3cd837ddf8e4c57f05d70ab865de6e193bbb":"QmTVCVgi4QXPLGEKNHLfBHxRGQCQqCFuo6wdsKVprqk5yh","0xdde2d979e8d39bb8416eafcfc1758f3cab2c9c72":"Qma7TGR1wVupWT4YufGs1NNAH786cccbjgo5rzfA8iCD3F","0xf7f6c2836293a661be2690fbacae97f3f027e9c4":"QmXGWKpgJGtxbmVALCs61fSsKjYobitPerKXipw2kDBv5b","0x41a322b28d0ff354040e2cbc676f0320d8c8850d":"QmdMu9Dfa6yAXUhQCnHkaYAgWiZqKnvNAMdEPoUPUe8Ps3","0xa92e3ab42c195e52c9fbf129be47ecbb03845dfd":"QmXAgUWwp3iujcsgJTEwYkebXwh8KEMMhLYX5mavFr4bSy","0x1b5242794288b45831ce069c9934a29b89af0197":"QmUBZBRtn6zkhfFKSk7HRNTmmJ4uAZTNgym8oMbMjmSHCV","0x6c0f03411ab250f0a080a4420190b821affd4602":"QmZd3ZjZoXbR7FrNxcKV2uivEYVwmjPWrngrtPn4z1MQmS","0x6e10e8f202ced220791043df74aa84615caec537":"QmPyaKPSL18heX68RTb8L78fQBYievHyNEdGhfGfUAvD5L","0x5d0ee539893323f9965e3ac2846ea6421255ae79":"QmY1aP1GoXSW629KRfDV4hfjeTq2WqBHs2wm79B4gRZJtf","0xbb5ed1edeb5149af3ab43ea9c7a6963b3c1374f7":"QmUG3umSeqnxgNEuoND3aQCsrXUHc6jVwb2YZ6n1WpX3eq","0xdcaad9fd9a74144d226dbf94ce6162ca9f09ed7e":"QmPamx3pAADvBYji3V6mKet357qqZYXqCaN6qL4cSCiEZ5","0xd73be539d6b2076bab83ca6ba62dfe189abc6bbe":"QmeL7ypRLoNKSQpGWXo83RdvosnwPsGkTip9NCYFvn9Uxw","0xbf8a84de5dcc0bd5792026bfdebfc75d9675a363":"QmUk9DhwqWoRcv3Erj2coBPsMFYVq14u2yxGi8eJ1TbkhU","0x7490a548fb5c4d1066f5e3dd315bf0c5ba79b169":"QmSXaNUWQwV9m4A2LA8gK6sdu7fZuu4bUMeDvaSGemPDgp","0x7fdcd2a1e52f10c28cb7732f46393e297ecadda1":"QmQ9kxn5dapKMkei14JqbY3qWDrFRrZQ9KKigZAPCs1CfQ","0x2d9e5de7d36f3830c010a28b29b3bdf5ca73198e":"QmYuaN2rFUXZQ61AARShM1aRJQcVMB2pxoH29D3evr9HPv","0x323a3e1693e7a0959f65972f3bf2dfcb93239dfe":"QmYfnQsGKuNg65ojJnL54RbjWXJ6BccpcKnUBzPwEu3zz6","0x14b2d558687942834839874d33a73c852be5401d":"QmfGHiEaaUuCBMcb2Yp6Y4y9oaaCK7mwrzrwWa9HoeUSHb","0x663e4229142a27f00bafb5d087e1e730648314c3":"QmThZGyv48hffc7Skcjhq89F5TK3hFRNcd1jeaqtez2G6X","0x06a6a7af298129e3a2ab396c9c06f91d3c54aba8":"QmTdnaa1ssJ1GModUZkCUChUyiRabrQqMxpP9cL2gqku4U","0xbace7e22f06554339911a03b8e0ae28203da9598":"QmUahva5q5DCDKZ8WEJE1cV46hgrkPXzCyCa6A674qV8kk","0x11bdfb09bebf4f0ab66dd1d6b85d0ef58ef1ba6c":"QmYuaN2rFUXZQ61AARShM1aRJQcVMB2pxoH29D3evr9HPv","0x79986af15539de2db9a5086382daeda917a9cf0c":"Qmcmjokb6XCD8FfXdtGMXDxqi7GEXsHSuEP6kNhbHHWeW4","0xe272fddbd056240149c771f9fd917fa040dceb39":"QmUaNt2LNQgWeYYKVFc2zzwtxdQgUVTBBgsAxH6vCxBBfE","0x1bbc9c79e7bdfd5a384e1121d169cf89e59d170b":"QmeVuJMR1CfUpELv33aQQLX6XPvmMit7zVQvMkBa6FA2iE","0x739039c187655590367f96158201c941a61d440a":"QmboHRBLvBuphMg44AYDq1hrxYne52LkZwB5E7QgSyFnab","0xf5b0a3efb8e8e4c201e2a935f110eaaf3ffecb8d":"QmQ712KPPmk9mYqLQfDHFEEniffA2QjujJcCCaoxeoM8rA","0x92cb5f1fbabbcbdd891b9cbd8e9a056c8c1eebef":"QmVYn4XyUAtYhd5vCgP834TpisJh1HcpBqxgw9daj5sVMB","0xb77f7a9c7018f217ee80a1eece0766956b8fffb7":"Qmbij7Yae8SvWeL75LEignYAcbPEXkCxYB7PtVMvoLZuCs","0x772da237fc93ded712e5823b497db5991cc6951e":"QmY19tX1HYpRaY4yLkjag5V1x9imkyZLpeLDkZXXbMW9WY","0x0a2ea71d943bf917b410593194595e1f48d40e54":"QmbmPH2dLQmSfCPyJAFKR5n9mdLmLrS3QVVrhm3MzgSHoG","0x5d00d312e171be5342067c09bae883f9bcb2003b":"QmRnpF56LbtgucGdr5M8fWcW6mDN22qYokKAe4wF9x31TR","0xcddcc63883683a3e6d5537d0b47074c3accc790e":"QmYiw8neSFevVygu2yZMv5kVPzycBFak66qQrTeLcexePW","0x6ebeaf8e8e946f0716e6533a6f2cefc83f60e8ab":"QmYdAVW6sgeuZfhLJDAMY3i6ZwWK5awG1ohBgPZYbbySG2","0xf87e31492faf9a91b02ee0deaad50d51d56d5d4d":"QmYWpBHPhC4LMVAyZT7VCcA8Dc1hhjcRvsCaxsXCxtehSV","0x71c118b00759b0851785642541ceb0f4ceea0bd5":"QmdcR4a9Ld3fUXAdXFRNUjbPRjHxZJRZszcbmnj3N1A5s8","0xd4202b234c15255bf0511d3380e83bda9172a72b":"QmcfJESCFHA8MJSfiSLNaC8tgAQAXg59dXebeY1NTATGEg","0xf42ed50058b88a063f8bb4060fa6dc269d90b5b9":"QmU2C6QnYj81dhQuJDatgrsSCwsRaQgwBPh6dP72xA5ZTV","0x6ce8fb3149d5186d7021a48430a7c7d49ce4455a":"QmT9CWiBgy9iuiYySpGwAHtFkqDD9SGo5PC97vEZfE92pK","0x58e80f54c86b6df98814cc274893534c0f7785e8":"QmdLTnwq9rhBxC45W2QZbmac2jpCpCcDrBgnfSSTYeVFzN","0xb71b092367b638b93bac3e0a2d5c7f411e9c89be":"QmRnAL5dKxYT7WYNrEzbrJecvAKB1NN84WBUHZoUR1tPHf","0x1a94fce7ef36bc90959e206ba569a12afbc91ca1":"QmS53pVgryhtiHHyuH6WRUC8Z6s5sMuxaFYeh3xfKGxqPk","0x2e7570255ac8c449217fb15405fac01f5408d114":"QmbnBkTW9egnkUEJmJse5kuVztYtPXazTLK6pDH4P3myfZ","0x69b47e2bc52fa28fef7bae5a89a0421126bd56c9":"QmUXtnryxCnRJdFdKMxehhgsTiwnc3iDFjwrLsrQ9z44xv","0xdfe0e36ea59dd7bcf8e72b3aa38d7ff1f472f1ce":"QmPKjxD2SiWyDFHvUJc9EdavhgJCafUinPTTiR9G61SPCn","0x4d3814d4da8083b41861dec2f45b4840e8b72d68":"QmPjzGaK42zsoTM3Fn5uioftLDXccsxWtfiu5bg9iVYFTW","0xe4f5e0d5c033f517a943602df942e794a06bc123":"QmV2Xp59Bx9YhPusCiHtvVfrK14MPodiuF3LBkz5G2M2k6","0x6338191071747abc74d7644d71b49c07289b1eff":"QmVbNKj4EWcauc3VvrMwLhyqvxLz3KWEJHbxfphZqfYvD1","0x1b3237bdfe285fa01b9a01c6d0f66b10805e6092":"QmU2C6QnYj81dhQuJDatgrsSCwsRaQgwBPh6dP72xA5ZTV","0x8c9b261faef3b3c2e64ab5e58e04615f8c788099":"QmTZaaok5P7QUBMS8pSVE5T6y8uYjHTGn7uREQFq5d83Lj","0xcc9a66acf8574141b0e025202dd57649765a4be7":"QmPjzGaK42zsoTM3Fn5uioftLDXccsxWtfiu5bg9iVYFTW","0x271a0b465d5b453bb835afd2d671c76b2b76900e":"QmdFke4Lkn3kNNxd6VsEoQ25ZyJ2QKAef38V3C41YJopkF","0xc1eab49cf9d2e23e43bcf23b36b2be14fc2f8838":"QmQNGRKvu3Pw6hU6HiMWMGTEhxp5TXSiQeXNX133fj7JhY","0xf766b3e7073f5a6483e27de20ea6f59b30b28f87":"QmcrKJhNaYM9rEBKAikm6nWpv4x4BFdJJ2F3Wca786bp5j","0x959e104e1a4db6317fa58f8295f586e1a978c297":"QmYWpBHPhC4LMVAyZT7VCcA8Dc1hhjcRvsCaxsXCxtehSV","0x4fece400c0d3db0937162ab44bab34445626ecfe":"Qme428RatHR4YxQ1tz4i4tQCYkD3KF8gswBPZRzJWK5kgV","0x86e4dc25259ee2191cd8ae40e1865b9f0319646c":"QmNr9GwX15XZdEFVNHAqYCTHY2R17x4SvrjcvJ2odBvjqR","0xe264d16bcba50925d0e1a90398596ec010306e14":"QmNWaUobNsx4qCEsY65YJMhvqa1c2Ay2GnbaPcpyupHsVD","0xc95c0910d39d1f6cd3bd71e4b689660c18172b7b":"QmaMQxAfk7KbicYWmwXVw21VjyzhVejj5ctC8fHTccLxg5","0xe9df89e2e7df9064ce85a0099d731eabaf8dc860":"QmerPLNNnWArU4chqZLSfReAhhmWJBj6QR1qB5foWLXWDC","0xb5da84cdc928765c15a8192bf3c6649e7802772b":"QmdJNz9ihBPvFWGjYPSm8egsbS22aW4qUAG6ErhYNt7Xno","0xf97187f566ec6374cb08470cce593ff0dd36d8a9":"QmNi4XVuSEXhcoXiMbBoVb5NG2pz5oVU2zoZ3mSA27Gh6d","0x7c93f0e4096dceec5dca5c21124c1dff5d0c2be5":"QmQPH6cKpWoycKUXEiDR6SwDEkqCNFUThiunutZawfnv79","0xa33ab4b0c9905ebc4e0df5eb2f915bee728b8253":"QmbG4fG4PN3VJPRHboyyyLCcFTccpu2u7LDybgfFyfRqxL","0x89be28ea3b9a24885e343674f25cd8444fd692e5":"QmQPH6cKpWoycKUXEiDR6SwDEkqCNFUThiunutZawfnv79","0x7d152fe26431e6585dbff9ef37e375e49b862739":"QmPWP2vsZUvrFdCEpFbthti5CaCwpWmbmkWUT9gMi8crdo","0xa98ad92a642570b83b369c4eb70efefe638bc895":"QmU7BmMmmHYMYVFdzhpDJCgNGW8KwiAqtvHqbYcSZt1SHB","0xe025523bff25d354e4fda8c6d0c514fc50390482":"QmQPH6cKpWoycKUXEiDR6SwDEkqCNFUThiunutZawfnv79","0x58124afeb40f51bec09cba79f3ca76f7ae05939f":"QmeiVSQZrkSpFJGhoaFb29cBfV9XAxY1e9m1Bi7Jb6Dd1c","0x273f7f8e6489682df756151f5525576e322d51a3":"QmPhTu8b5iw8KbWZTWWAfzfLoTdiikFf3hZ5AXEMNxZmgz","0x40b41a2d4c5fb2da3e08b0a1bbd361b5dd1f7bdd":"QmXhZVxKwLANgvXjF2H5RGZriUhahpmaN7nPpTZa1K1Jhi","0xc70be5b7c19529ef642d16c10dfe91c58b5c3bf0":"QmWjaSN4uzT8UXivcD3FJQVWUuHJaYKLY464DjvXzxRcFF","0xbfde6246df72d3ca86419628cac46a9d2b60393c":"QmWtA4WHZTFHkSqrCjS5i6KohtNDHZEy6C4WS9xXxyLd1b","0x9d9c250311b65803c895cc77f878b8092019dedc":"QmeXtn7coRE9N272n5MdYAZiNDojWzrS22SLxpgdEzQ6XK","0xbc5563de646ca6c6d7e2f7cc44ffadf1b5b614ef":"QmShrDsZRahyKC4wYijK6zxH2nStUtqSX54VHTUZq17u9a","0xaafba0555828c98216a7254b8500043ab27ce4f0":"QmQMheZbxmJbpyuBdsCDFpjzakrVfsyvweXotArLuGMumS"}

/***/ })
/******/ ]);
//# sourceMappingURL=main.map