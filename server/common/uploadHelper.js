const fs = require('fs')
const path = require('path')
const ipfsApi = require('ipfs-api')

const ipfs = ipfsApi('ipfs.infura.io', '5001', { protocol: 'https' })

// file to help keep track of statically uploaded test files
const jsonManifest = require('./nftFilteredData.json')
const manifestPath = path.join(__dirname, 'jsonManifest.json')

// LOGIC:
// 1. read addresses from current tokens folder
// 2. write all addresses to jsonManifest
// 3. grab buffer image data for each token
//  3b. upload to IPFS
//  3c. store hash in jsonManifest
if (Object.keys(jsonManifest).length <= 0) {
  const tokenFolder = path.join(__dirname, 'nftimgs')
  const files = fs.readdirSync(tokenFolder)
  console.log('files.length', files.length)

  files.forEach(f => {
    const k = f.split('.')[0]
    if (k.substring(0, 2) === '0x') jsonManifest[k] = '-'
  })

  fs.writeFile(manifestPath, JSON.stringify(jsonManifest), 'utf8', () => {
    console.log('updated jsonManifest!', manifestPath)
  })
}

// upload setup
const errors = []
const promises = []
const hashes = Object.keys(jsonManifest)

if (hashes.length <= 0) return
const uploadHash = h => {
  return new Promise((resolve, reject) => {
    const hashFile = path.join(__dirname, 'nftimgs', `${h}.png`)
    fs.readFile(hashFile, (error, data) => {
      if (error) return reject(error)
      ipfs.files.add(data, (err, files) => {
        if (err) {
          errors.push(h)
          return reject(err)
        }
        const imgHash = files[0].hash
        jsonManifest[h] = imgHash
        console.log(`uploaded ${h}.png - ${imgHash}`)
        // incremental update, since this is sequential and takes a while
        fs.writeFileSync(manifestPath, JSON.stringify(jsonManifest), 'utf8')

        resolve(imgHash)
      })
    })
  })
}

const hs = hashes.reduce((p, val) => p.then(() => uploadHash(val)), Promise.resolve())

hs.then(res => {
  console.log('errors', errors)
  console.log('Upload Complete!')
  // fs.writeFile(manifestPath, JSON.stringify(jsonManifest), 'utf8', () => {
  //   console.log('updated jsonManifest!', manifestPath)
  // })
})
.catch(console.error)
