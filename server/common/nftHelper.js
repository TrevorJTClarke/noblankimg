const fs = require('fs')
const path = require('path')
const https = require('https')
// const request = require('request')
// const ipfsApi = require('ipfs-api')

// const ipfs = ipfsApi('ipfs.infura.io', '5001', { protocol: 'https' })

// file to help keep track of statically uploaded test files
const nftRawJson = require('./tmpNftData.json')
const nftFilteredPath = path.join(__dirname, 'nftFilteredData.json')
const tmpNfts = {}

if (nftRawJson.length > 0) {
  nftRawJson.forEach(f => {
    const a = f.address
    const img = f.featured_image_url || f.image_url
    if (img) tmpNfts[a] = { cover: img, n: f.name }
  })

  console.log('tmpNfts total', Object.keys(tmpNfts).length)

  fs.writeFile(nftFilteredPath, JSON.stringify(tmpNfts), 'utf8', () => {
    console.log('updated nftFilteredPath!', nftFilteredPath)
  })
}

const upload = (key, url) => {
  https.get(url, function(response) {
    var imageData = ''
    response.setEncoding('binary')
    response.on('data', function(chunk){
      imageData += chunk
    })

    response.on('end', function() {
      const imgPath = path.join(__dirname, 'nftimgs', `${key}.png`)
      fs.writeFile(imgPath, imageData, 'binary', function(error){
        if (error) {
          console.log(error)
        } else {
          console.log(`Success: ${tmpNfts[key].name} ${key}`)
        }
      })
    })
  })
}

// NOTE: Only enable if new images
// const imgNfts = Object.keys(tmpNfts)
// if (imgNfts.length > 0) {
//   imgNfts.forEach(k => {
//     upload(k, tmpNfts[k].cover)
//   })
// }
