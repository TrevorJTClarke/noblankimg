import assetRouter from './api/modules/assets/router'

const baseUri = '/v1'

export default function routes(app) {
  app.use(`${baseUri}/assets`, assetRouter)
}
