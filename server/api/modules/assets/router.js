import * as express from 'express'
import multer from 'multer'
import controller from './controller'

// handle multi-part
const upload = multer()

export default express
  .Router()
  .post('/', upload.any(), controller.create)
  .get('/:id/fallback', controller.byIdImageFallback)
  .get('/:id/meta', controller.metaById)
  .get('/:id', controller.byId)
  .patch('/:id', controller.updateId)
  .delete('/:id', controller.removeId)
