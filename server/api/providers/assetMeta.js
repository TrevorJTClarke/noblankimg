const getColors = require('get-image-colors')

class AssetMetaProvider {
  constructor() {
    return this
  }

  async image(options) {
    const colors = await getColors(options.path, 'image/png')
    return colors.map(color => color.hex())
  }
}

export default AssetMetaProvider
