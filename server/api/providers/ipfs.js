import ipfsApi from 'ipfs-api'
import multihash from '../../common/multihash'

// Looks like IPFS on infura doesn't require API key yet
const apiKey = process.env.INFURA_API_KEY || ''

// TODO: Might not needed
// const fileOptions = {
//   pin: true,
//   wrapWithDirectory: 'test'
// }

class IpfsProvider {
  constructor() {
    this.baseUrl = 'https://ipfs.infura.io:5001/api/v0/cat?arg='
    this.ipfs = ipfsApi('ipfs.infura.io', '5001', { protocol: 'https' })
    return this
  }

  async upload(bufferData) {
    return new Promise((resolve, reject) => {
      this.ipfs.files.add(bufferData, async (err, files) => {
        if (err) return reject(err)
        const hash = files[0].hash
        const fileData = await multihash.getBytes32FromMultiash(hash)
        fileData.hash = hash

        resolve(fileData)
      })
    })
  }

  async byHash(hash) {
    return new Promise((resolve, reject) => {
      try {
        this.ipfs.files.get(hash, (err, files) => {
          if (err) return resolve(null)
          return resolve(files)
        })
      } catch (e) {
        reject(e)
      }
    })
  }

  getUrl(hash) {
    return `${this.baseUrl}${hash}`
  }
}

export default IpfsProvider
