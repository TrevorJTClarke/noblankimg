import fs from 'fs'
import path from 'path'
import Web3 from 'web3'

const godAddress = '0x0000000000000000000000000000000000000000000000000000000000000000'
const ContractAbiPath = path.join(
  __dirname,
  '../../../',
  'build/contracts/NoBlankImg.json',
)

// Get an API key from https://infura.io/
const apiKey = process.env.INFURA_API_KEY || ''
const network = process.env.ETH_NETWORK || ''
const rpcUrl = () => {
  if (process.env.NODE_ENV !== 'production') return `http://localhost:8545`
  return `https://${network}.infura.io/v3/${apiKey}`
}

const getAbiDeployedAddress = abi => {
  const networks = abi.networks
  return networks[Object.keys(networks)[0]].address
}

class Web3Provider {
  constructor() {
    this.web3 = new Web3(new Web3.providers.HttpProvider(rpcUrl()))

    try {
      this.AbiFile = JSON.parse(fs.readFileSync(ContractAbiPath, 'utf8'))
      if (this.AbiFile)
        this.deployedAddress = getAbiDeployedAddress(this.AbiFile)
        this.Contract = new this.web3.eth.Contract(this.AbiFile.abi, this.deployedAddress)
    } catch (e) {
      console.log('e', e)
      console.error(
        'Could not load contract ABI file, please check address or redeploy!',
      )
    }

    return this
  }

  getHashReference(hash) {
    return new Promise((resolve, reject) => {
      // this.Contract.methods.verifiedAsset(hash).call((e, r) => {
      //   if (e) return reject(e)
      //   if (r === godAddress) return resolve(null)
      //   resolve(r)
      // })
      this.Contract.methods.getAssetByHash(hash).call((e, r) => {
        if (e) return reject(e)
        console.log('r', r['0'], r['1'], Web3.utils.hexToAscii(r['0']))
        const hashRef = r['0']
        const verified = r['1'] !== godAddress && r['1'] === hashRef
        resolve({
          hash: hashRef,
          verified
        })
      })
    })
  }
}

export default Web3Provider
