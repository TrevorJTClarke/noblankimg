pragma solidity ^0.4.18;

import './Ownable.sol';


/**
 * @title NoBlankImg
 * @dev The NoBlankImg contract contains methods for managing admins, peers,
 * and asset hashes.
 */
contract NoBlankImg is Ownable {

    event AssetAdded(address indexed assetId, bytes32 digest, address uploader);
    event AssetRemoved(address indexed assetId, bytes32 digest, address uploader);
    event AssetVerified(address indexed assetId, bytes32 digest, address peer);
    event AssetUnverified(address indexed assetId, bytes32 digest, address peer);
    event PeerEnlisted(address indexed peer, address admin);
    event PeerDelisted(address indexed peer, address admin);
    event AdminEnlisted(address indexed enlistedAdmin, address admin);
    event AdminDelisted(address indexed delistedAdmin, address admin);

    struct Peer {
        bool isEnlisted;
        // CONSIDER: Peer time since enlisted
    }

    struct Asset {
        bool exists;
        bytes32 digest;
        uint8 hashFunction;
        uint8 size;
        // CONSIDER: A mapping of peers who flagged this asset
    }

    struct Admin {
        bool isAdmin;
    }

    // TODO: Source: See https://github.com/multiformats/multihash

    /*    struct Multihash {
            bytes32 digest;
            uint8 hashFunction;
            uint8 size;
        }*/


    mapping(address => Asset) public verifiedAsset;
    mapping(address => Asset) public unverifiedAsset;
    mapping(address => Peer) public peer;
    mapping(address => Admin) public admin;

    modifier only_admin {
        require(admin[msg.sender].isAdmin == true, "Address is not admin");
        _;
    }

    modifier only_peer {
        require(peer[msg.sender].isEnlisted == true, "Address is not an enlisted peer");
        _;
    }

    /**
     * @dev The NoBlankImg constructor sets the admin to the contract owner
     * (the contract deployer)
     */
    constructor() public {
        admin[owner()].isAdmin = true;
        peer[owner()].isEnlisted = true;
    }

    function enlistAdmin(address _admin) public only_admin {
        admin[_admin].isAdmin = true;
        peer[_admin].isEnlisted = true;
        emit AdminEnlisted(_admin, msg.sender);
    }

    function delistAdmin(address _admin) public only_admin {
        delete admin[_admin];
        delete peer[_admin];
        emit AdminDelisted(_admin, msg.sender);
    }

    function verifyAsset(address assetId) public only_peer {
        require(unverifiedAsset[assetId].exists, "Asset doesn't exist");

        // Add the unverified asset to the list of verified
        // assets and remove it from the unverified assets

        Asset memory asset = Asset(true,
            unverifiedAsset[assetId].digest,
            unverifiedAsset[assetId].hashFunction,
            unverifiedAsset[assetId].size);

        verifiedAsset[assetId] = asset;
        delete unverifiedAsset[assetId];
        emit AssetVerified(assetId, verifiedAsset[assetId].digest, msg.sender);
    }

    function unverifyAsset(address assetId) public only_admin {
        require(verifiedAsset[assetId].exists, "Verified asset doesn't exist");
        Asset memory asset = Asset(true,
            unverifiedAsset[assetId].digest,
            unverifiedAsset[assetId].hashFunction,
            unverifiedAsset[assetId].size);
        unverifiedAsset[assetId] = asset;
        delete verifiedAsset[assetId];
        emit AssetUnverified(assetId, unverifiedAsset[assetId].digest, msg.sender);
    }

    function addAsset(address assetId, bytes32 digest, uint8 hashFunction, uint8 size) public {
        require(!unverifiedAsset[assetId].exists, "Asset already exists");
        unverifiedAsset[assetId] = Asset(true, digest, hashFunction, size);
        emit AssetAdded(assetId, digest, msg.sender);
    }

    function getAssetByHash(address assetId) public view returns (bytes32, uint8, uint8, bool) {
        if (verifiedAsset[assetId].exists) {
            return (verifiedAsset[assetId].digest,
                    verifiedAsset[assetId].hashFunction,
                    verifiedAsset[assetId].size, true);
        } else {
            return (unverifiedAsset[assetId].digest,
                    unverifiedAsset[assetId].hashFunction,
                    unverifiedAsset[assetId].size, false);
        }
    }

    function removeAsset(address assetId) public only_admin {
        require(unverifiedAsset[assetId].exists, "Unverified asset doesn't exist");
        bytes32 digest = unverifiedAsset[assetId].digest;
        delete unverifiedAsset[assetId];
        delete verifiedAsset[assetId];
        emit AssetRemoved(assetId, digest, msg.sender);
    }

    function enlistPeer(address _peer) public only_admin {
        require(!peer[_peer].isEnlisted, "Peer already enlisted");
        peer[_peer] = Peer({isEnlisted : true});
        emit PeerEnlisted(_peer, msg.sender);
    }

    function delistPeer(address _peer) public only_admin {
        require(peer[_peer].isEnlisted == true, "Peer does not enlisted");
        delete peer[_peer];
        emit PeerDelisted(_peer, msg.sender);
    }
}
