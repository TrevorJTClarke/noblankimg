/* eslint-disable no-undef */
const catchRevert = require('../helpers/exceptions.js').catchRevert

const NoBlankImg = artifacts.require('./NoBlankImg')
const getBytes32FromMultiash = require('../helpers/multihash.js')
  .getBytes32FromMultiash
const getMultihashFromContractResponse = require('../helpers/multihash.js')
  .getMultihashFromContractResponse

// "0x"+bs58.decode(ipfsListing).slice(2).toString('hex')

contract('NoBlankImg', async accounts => {
  const OG_ADMIN = accounts[0]
  const NEW_ADMIN = accounts[1]
  const NON_ADMIN = accounts[2]
  const NEW_PEER = accounts[3]

  it('should initialize the admin as the contract deployer', async () => {
    const instance = await NoBlankImg.deployed()
    const isAdmin = await instance.admin.call(OG_ADMIN)
    assert.equal(isAdmin, true)
  })

  it('should allow admin to enlist a new admin', async () => {
    const instance = await NoBlankImg.deployed()
    // Enlist a new admin
    await instance.enlistAdmin(NEW_ADMIN, { from: OG_ADMIN })

    // Check if newly enlisted admin is indeed admin
    const isAdmin = await instance.admin.call(NEW_ADMIN)
    assert.equal(isAdmin, true)
  })

  it('should not allow non admins to delist admins', async () => {
    const instance = await NoBlankImg.deployed()
    await catchRevert(instance.delistAdmin(NEW_ADMIN, { from: NON_ADMIN }))
  })

  it('should allow admin to delist an admin', async () => {
    const instance = await NoBlankImg.deployed()

    // Delist a the new admin
    await instance.delistAdmin(NEW_ADMIN, { from: OG_ADMIN })

    // Check if delisted admin is nolonger an admin
    const isAdmin = await instance.admin.call(NEW_ADMIN)
    assert.equal(isAdmin, false)
  })

  it('should not allow non admins to enlist new admins', async () => {
    const instance = await NoBlankImg.deployed()
    await catchRevert(instance.enlistAdmin(NEW_ADMIN, { from: NON_ADMIN }))
  })

  it('should only allow admins to enlist new peers', async () => {
    const instance = await NoBlankImg.deployed()
    await instance.enlistPeer(NEW_PEER, { from: OG_ADMIN })

    // Check if newly enlisted peer is indeed a peer
    const isPeer = await instance.peer.call(NEW_PEER)
    assert.equal(isPeer, true)
  })

  it('should not allow non admins to delist a peer', async () => {
    const instance = await NoBlankImg.deployed()
    await catchRevert(instance.delistPeer(NEW_PEER, { from: NON_ADMIN }))
  })
  it('should allow admin to delist a peer', async () => {
    const instance = await NoBlankImg.deployed()

    // Delist a the new peer
    await instance.delistPeer(NEW_PEER, { from: OG_ADMIN })

    // Check if delisted peer is nolonger a peer
    const isPeer = await instance.peer.call(NEW_PEER)
    assert.equal(isPeer, false)
  })

  it('should not allow non admins to enlist new peers', async () => {
    const instance = await NoBlankImg.deployed()
    await catchRevert(instance.enlistPeer(NEW_PEER, { from: NON_ADMIN }))
  })
})

contract('NoBlankImg--Assets', async accounts => {
  const OG_ADMIN = accounts[0]
  // const NEW_ADMIN = accounts[1]
  // const NON_ADMIN = accounts[2]
  // const NEW_PEER = accounts[3]
  const ASSET_ID = accounts[4]
  const ipfsHashes = [
    'QmahqCsAUAw7zMv6P6Ae8PjCTck7taQA6FgGQLnWdKG7U8',
    'Qmb4atcgbbN5v4CDJ8nz5QG5L2pgwSTLd3raDrnyhLjnUH',
  ]

  let noBlankImg

  beforeEach(async () => {
    noBlankImg = await NoBlankImg.new()
  })

  async function addAsset(account, assetId, hash) {
    const { digest, hashFunction, size } = getBytes32FromMultiash(hash)
    return noBlankImg.addAsset(assetId, digest, hashFunction, size, {
      from: account,
    })
  }

  async function getAssetHash(assetId) {
    const [
      digest,
      hashFunction,
      size,
      isVerified,
    ] = await noBlankImg.getAssetByHash(assetId)
    return getMultihashFromContractResponse([digest, hashFunction, size])
  }

  it('should get asset hash after adding it', async () => {
    await addAsset(OG_ADMIN, ASSET_ID, ipfsHashes[0])

    assert.equal(await getAssetHash(ASSET_ID), ipfsHashes[0])
  })

  // it('Can not add duplicate asset', async () => {
  //   const instance = await NoBlankImg.deployed()
  //
  //   await catchRevert(
  //     instance.addAsset(ASSET_ID, IPFS_HASH, { from: OG_ADMIN }),
  //   )
  // })
})
