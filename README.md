# noblankimg

No image should be caught missing, fallbacks and meta data provide better experience for users!

## Start Development & Testing Locally

1. Clone the repo and run `npm install`

The repo development is mainly in `/api`, `/src` and `/test`.

#### To run local development website:
`npm run dev` - Starts server & watches folders

#### Helpful Commands
- `npm run build` - Builds compiled client deployable package
- `npm test` - Runs all tests
- `truffle compile` - Test contracts are compiling and have no errors
- `truffle migrate` - Compile and deploy contracts to local blockchain
- `truffle migrate --network rinkeby` - Compile and deploy contracts to network
- `truffle migrate -f 2 --network rinkeby` - Compile and deploy contracts to network without Migrations.sol

## API Documentation

The following are endpoints, allowing access to image & other asset resources

### **GET** /assets/:hash{type}

##### Request

Example: `https://noblankimg.com/v1/assets/0x000000000000000000000000000000000000.png`

##### Response

Returns Image Mime type based on `{type}`

### **GET** /assets/:hash/meta

TODO: Query param options for sizing

##### Request

Example: `https://noblankimg.com/v1/assets/0x000000000000000000000000000000000000/meta`

##### Response

```json
{
  "brand": ["#000000", "#333333"],
  "assetPath": "https://ipfs.io/hash.png",
  "network": {
    "id": "fdsa",
    "slug": "ethereum-mainnet"
  },
  "addressType": "contract",
  "contractType": ["ERC20"],
  "verified": false
}
```

### **POST** /assets/:hash

TODO: Spec for user submitting asset

### **PATCH** /assets/:hash

TODO: Spec for user updating asset

### **DELETE** /assets/:hash

TODO: Spec for user deleting asset
